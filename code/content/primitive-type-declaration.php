<pre>
/**
 * Primitive type declarations
 *
 * Type coercion
 * By defining the functions arguments datatype, we tell the complier to convert the
 * provided arguments to that data type. if the coercion fails it will throw a fatal error.
 */
  function parse(int ...$ints) {
    echo json_encode($ints, JSON_PRETTY_PRINT);
  }
  parse(20, '200', '200.33', 22.22, true, '22,000.34', 0);
</pre>

<?php
function parse(int ...$ints) {
  echo '<pre>', json_encode($ints, JSON_PRETTY_PRINT), '</pre>';
}
parse(20, '200', '200.33', 22.22, true, '22,000.34', 0);
?>

<pre>
  function parse2(int $int, string $string, float $float, bool $bool) {
    echo json_encode(array($int, $string, $float, $bool), JSON_PRETTY_PRINT);
  }
  parse2(20, '200', 22.22, true);
</pre>

<?php
function parse2(int $int, string $string, float $float, bool $bool) {
  echo '<pre>', json_encode(array($int, $string, $float, $bool), JSON_PRETTY_PRINT), '</pre>';
}

parse2(20, '200', 22.22, true);

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>What's New in PHP 7</title>
</head>
<body>
  <h2>What's New in PHP 7</h2>
  <nav>
    <h3>PHP 7 New Features</h3>
    <ul>
      <li>
        <a href="content/primitive-type-declaration.php">Primitive type declarations</a>
      </li>
    </ul>
  </nav>
</body>
</html>